package net.qqxh.controller;

import net.qqxh.common.utils.SvnUtil;
import net.qqxh.controller.common.FileViewRouter;
import net.qqxh.persistent.JfUserSimple;
import net.qqxh.persistent.Jfile;
import net.qqxh.persistent.SearchLib;
import net.qqxh.service.FileEsService;
import net.qqxh.service.FileService;
import net.qqxh.service.task.FileResolveTaskCallBack;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.index.IndexNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.tmatesoft.svn.core.SVNDepth;
import org.tmatesoft.svn.core.SVNURL;
import org.tmatesoft.svn.core.internal.io.svn.SVNRepositoryFactoryImpl;
import org.tmatesoft.svn.core.internal.wc.DefaultSVNOptions;
import org.tmatesoft.svn.core.wc.ISVNOptions;
import org.tmatesoft.svn.core.wc.SVNClientManager;
import org.tmatesoft.svn.core.wc.SVNRevision;
import org.tmatesoft.svn.core.wc.SVNWCUtil;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/svn")
public class SvnController extends BaseController {

    @RequestMapping("/update")
    public Object update(@RequestParam(name = "path") String path) {
        JfUserSimple jfUserSimple = getLoginUser();
        SearchLib searchLib=jfUserSimple.getSearchLib();
        if(StringUtils.isEmpty(searchLib.getSvnRepository_url())){
            return responseJsonFactory.getSuccessJson("更新成功", "");
        }
        SVNRepositoryFactoryImpl.setup();
        //相关变量赋值
        ISVNOptions options = SVNWCUtil.createDefaultOptions(true);
        SVNClientManager ourClientManager = SVNClientManager.newInstance((DefaultSVNOptions) options, searchLib.getSvnUsername(), searchLib.getSvnPassword());
        File wcDir = new File(path);
        long workingVersion = -1;
        try {
            boolean isworkcopy = SvnUtil.isWorkingCopy(wcDir);
            if (!isworkcopy) {
                checkout();
            }
            workingVersion = SvnUtil.update(ourClientManager, wcDir, SVNRevision.HEAD, SVNDepth.INFINITY);
        } catch (Exception e) {
            e.printStackTrace();
            return responseJsonFactory.getErrorJson("更新失败", "");
        }

        return responseJsonFactory.getSuccessJson("更新成功", "");
    }

    @RequestMapping("/checkout")
    public Object checkout() {
        JfUserSimple jfUserSimple = getLoginUser();
        SearchLib searchLib=jfUserSimple.getSearchLib();
        SVNRepositoryFactoryImpl.setup();
        //相关变量赋值
        SVNURL repositoryURL = null;
        try {
            repositoryURL = SVNURL.parseURIEncoded(searchLib.getSvnRepository_url());
        } catch (Exception e) {
            //
        }
        ISVNOptions options = SVNWCUtil.createDefaultOptions(true);
        SVNClientManager ourClientManager = SVNClientManager.newInstance(
                (DefaultSVNOptions) options, searchLib.getSvnUsername(), searchLib.getSvnPassword());
        //要把版本库的内容check out到的目录
        File wcDir = new File(searchLib.getFileSourceDir());
        if(!wcDir.exists()){
            wcDir.mkdir();
        }
        long workingVersion = -1;
        try {
            if (SvnUtil.isWorkingCopy(wcDir)) {
                workingVersion = SvnUtil.update(ourClientManager, wcDir, SVNRevision.HEAD, SVNDepth.INFINITY);
            } else {
                workingVersion = SvnUtil.checkout(ourClientManager, repositoryURL, SVNRevision.HEAD, wcDir, SVNDepth.INFINITY);
            }

        } catch (Exception e) {
            e.printStackTrace();
            return responseJsonFactory.getErrorJson("检出失败", "");
        }
        return responseJsonFactory.getSuccessJson("更新成功", "");
    }

    @RequestMapping("/commit")
    public Object commit() {
        JfUserSimple jfUserSimple = getLoginUser();
        SearchLib searchLib=jfUserSimple.getSearchLib();
        SVNRepositoryFactoryImpl.setup();
        //相关变量赋值
        SVNURL repositoryURL = null;
        try {
            repositoryURL = SVNURL.parseURIEncoded(searchLib.getSvnRepository_url());
        } catch (Exception e) {

        }
        ISVNOptions options = SVNWCUtil.createDefaultOptions(true);
        SVNClientManager ourClientManager = SVNClientManager.newInstance(
                (DefaultSVNOptions) options, searchLib.getSvnUsername(), searchLib.getSvnPassword());
        //要把版本库的内容check out到的目录
        File wcDir = new File(searchLib.getFileSourceDir());
        try {
            SvnUtil.addEntry(ourClientManager, wcDir);
            SvnUtil.commit(ourClientManager, wcDir, false, "通过jfile提交");
        } catch (Exception e) {
            e.printStackTrace();
            return responseJsonFactory.getErrorJson("提交失败", "");
        }
        return responseJsonFactory.getSuccessJson("提交成功", "");
    }
}
