package net.qqxh.persistent;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.File;

@Entity
public class Jfile {
    @Id
    /*    @GeneratedValue*/
    private String rid;
    private String text;
    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String path;
    private String filePhase;
    @Column(nullable = false)
    private String viewPath;
    private String viewName;
    private String fix;
    private String viewFix;

    public Jfile(File file) {
        this.name = file.getName();
        this.path = file.getPath();
    }

    public Jfile() {
    }

    public String getFix() {
        String suffix = name.substring(name.lastIndexOf(".") + 1);
        return suffix;
    }

    public void setFix(String fix) {
        this.fix = fix;
    }

    public String getViewFix() {
        return viewFix;
    }

    public void setViewFix(String viewFix) {
        this.viewFix = viewFix;
    }

    public void setViewName(String viewName) {
        this.viewName = viewName;
    }

    public String getViewName() {
        File f = new File(this.viewPath);
        return f.getName();
    }

    public String getViewPath() {
        return viewPath;
    }

    public void setViewPath(String viewPath) {
        this.viewPath = viewPath;
    }


    public String getFilePhase() {
        return filePhase;
    }

    public void setFilePhase(String filePhase) {
        this.filePhase = filePhase;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getRid() {
        return rid;
    }

    public void setRid(String rid) {
        this.rid = rid;
    }
}
