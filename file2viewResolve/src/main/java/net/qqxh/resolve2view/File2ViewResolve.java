package net.qqxh.resolve2view;

public interface File2ViewResolve {
    /**
     * @param fromPath 源文件路径
     * @return view文件路径
     */
    public String resolve(String fromPath,String toPath);
    public boolean canResolve(String fileFix);
    public String getviewFix();
}
